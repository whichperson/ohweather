# OhWeather! 🌡
[![Netlify Status](https://api.netlify.com/api/v1/badges/6d5587e9-1aa6-410a-bdeb-dee7abd74a00/deploy-status)](https://app.netlify.com/sites/ohweather/deploys) [![Open Source Love svg1](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

OhWeather! is a lightweight mobile application. It provides a quick and easy way to check the current weather. 
You can either use your location or look up a different location using the city and country name. 

## Features 🌈
* Accurate local weather using location services
* Easy lookup by city, country name
* Temperature, humidity and conditions 
* Clean, simple UI

## Tools 🛠
* ReactJS
* [OpenWeather](https://openweathermap.org/api) API 
* Bootstrap 4 
* HTML, SCSS, JavaScript

## Usage
1. Allow user location services if you want the local weather
2. Type in the [City, Country] of your desired location 
3. Get the weather info

## General Info 
I built this app as my first ReactJS encounter, having previously worked with Vue.js. I wanted to "get my feet wet" but also create something that I would personally use. 
<br>Sidenote: This app was designed for mobile devices but also works on the web for demo purposes. 

## License
MIT Licensed © Meropi L. 2019
<br>You are free to clone/modify this repository with proper credit. 

### [Live Demo](https://ohweather.netlify.com/)





