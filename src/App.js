import React from "react";
import Weather from "./components/Weather";
import Search from "./components/Search";

const API_KEY = "1b042ee079a1356eb67a8d51bf38e37a";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.getCurrentWeather();
  };
  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    icon: undefined,
    error: undefined
  };

  expand() {
    let element = document.getElementById("search");
    let icon = document.getElementById("button");
    if ((!element.classList.contains("expand")) && (!icon.classList.contains("hide"))) {
      element.classList.add("expand");
      icon.classList.add("hide");
    } else {
      element.classList.remove("expand");
      icon.classList.remove("hide");
    }

  };

  getWeather = async e => {
    e.preventDefault();
    if (e.target.elements.location.value === '') {
      this.getCurrentWeather();

    } else {
      let location = e.target.elements.location.value.split(",");
      const city = location[0];
      const country = location[1].trim();
      const api_call = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`
      );
      const data = await api_call.json();
      if (city && country) {
        console.log(data);
        this.setState({
          temperature: parseInt(data.main.temp),
          city: data.name,
          country: data.sys.country,
          humidity: data.main.humidity,
          description: data.weather[0].description,
          icon: data.weather[0].icon,
          error: ""
        });
      } else {
        this.setState({
          city: undefined,
          country: undefined,
          humidity: undefined,
          description: undefined,
          icon: undefined,
          error: "Invalid City, Country"
        });
      }
    }

  };

  getCurrentWeather() {
    navigator.geolocation.getCurrentPosition(async e => {
      const longitude = e.coords.longitude;
      const latitude = e.coords.latitude;
      const api_call = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${API_KEY}&units=metric`
      );
      const data = await api_call.json();
      if (longitude && latitude) {
        console.log(data);
        this.setState({
          temperature: parseInt(data.main.temp),
          city: data.name,
          country: data.sys.country,
          humidity: data.main.humidity,
          description: data.weather[0].description,
          icon: data.weather[0].icon,
          error: ""
        });
      } else {
        this.setState({
          city: undefined,
          country: undefined,
          humidity: undefined,
          description: undefined,
          icon: undefined,
          error: "Please enable geolocation."
        });
      }
    });
  };

  render() {
    return (
      <div id="main-app">
        <div className="container shadow-sm">
          <div className="row m-0 justify-content-center">
            <div className="col-12">
              <div className="search">
                <Search
                  expand={this.expand}
                  getWeather={this.getWeather}
                />
              </div>
            </div>
          </div>
          <div className="row m-0 justify-content-center align-content-center">
            <div className="col-12">
              <div className="main-content">
                <Weather
                  icon={this.state.icon}
                  city={this.state.city}
                  country={this.state.country}
                  temperature={this.state.temperature}
                  description={this.state.description}
                  humidity={this.state.humidity}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default App;
