import React from "react";

const Search = props => (
  <div id="search-box">
    <form onSubmit={props.getWeather}>
      <input
        id="search"
        name="location"
        type="text"
        placeholder="City, Country"
        className="input"
      />
        <button className="btn" id="button" onClick={props.expand} type="button">
        <i className="fa fa-search" aria-hidden="true" />
      </button>
    </form>
  </div>
);

export default Search;
