import React from "react";

const Weather = props => (
    <div id="weather-box" className="justify-content-center align-items-center">
        {props.icon && <img className="weather-icon" alt="weather icon" src={(`/res/images/weather-icons/${props.icon}.svg`)}></img>}
        {props.city && <h2 className="city">{props.city}</h2>}
        {props.country && <h4 className="country">{props.country}</h4>}
        {props.temperature && <h1 className="temperature">{props.temperature}<span className="celsius">°C</span></h1>}
        {props.humidity && <p>Humidity: <span className="value">{props.humidity}%</span></p>}
        {props.description && <p>Conditions: <span className="value">{props.description}</span></p>}
        {props.error && <p>{props.error}</p>}
    </div>
);


export default Weather;
